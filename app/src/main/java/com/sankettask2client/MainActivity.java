package com.sankettask2client;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.sankettask2aidl.IMyAidlInterface;

public class MainActivity extends AppCompatActivity {
    IMyAidlInterface calService = null;
    TextView txt_value, txt_connection_status;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();
    }

    public void init() {
        txt_value = findViewById(R.id.txt_value);
        txt_connection_status = findViewById(R.id.txt_connection_status);
        if (calService == null) {
            Intent it = new Intent("multiplyservice");
            it.setPackage("com.sankettask2aidl");
            bindService(it, connection, Context.BIND_AUTO_CREATE);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbindService(connection);
    }

    private ServiceConnection connection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            calService = IMyAidlInterface.Stub.asInterface(service);
            try {
                txt_connection_status.setText("Service Connected ");
                txt_value.setText(String.valueOf(calService.getPitch()) + " " + String.valueOf(calService.getRoll()));
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            calService = null;
            txt_connection_status.setText("Service Disconnected ");
        }
    };
}